use pbin::types::*;
use quick_xml::events::Event::*;
use quick_xml::Reader;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use reqwest::{Client, Response};
use std::error::Error;
use std::fs::File;
use std::io;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use url::{form_urlencoded, Url};
use utils::deob;

pub mod types;

#[derive(Debug, Clone)]
pub struct PasteBinFactory {
    api_option: PasteBinOptions,
    dev_key: String,
    paste_key: Option<String>,
    paste_code: Option<String>,
    paste_private: Option<PasteBinPrivate>,
    paste_title: Option<String>,
    paste_expire_date: Option<PasteBinExpireDate>,
    paste_format: Option<PasteBinFormat>,
    user_key: Option<String>,
    user_key_cache: Option<PathBuf>,
    user_name: Option<String>,
    user_pass: Option<Vec<u8>>,
}

fn _should_login(err: &Box<Error>) -> bool {
    if err.description().contains("api_user_key") {
        true
    } else {
        false
    }
}

fn get_pbin_response(res: &mut Response) -> Result<String, Box<Error>> {
    let success = res.status().is_success();
    if success {
        if let Ok(msg) = res.text() {
            if msg.contains("api_user_key") {
                Err(Box::new(io::Error::new(io::ErrorKind::InvalidData, msg)))
            } else {
                Ok(msg)
            }
        } else {
            Err(Box::new(io::Error::new(
                io::ErrorKind::InvalidData,
                "BODY not available",
            )))
        }
    } else {
        Err(Box::new(io::Error::new(
            io::ErrorKind::ConnectionRefused,
            res.status().as_str(),
        )))
    }
}

//This should try and do a post firstly without login, if possible
pub fn try_post(pb_factory: &mut PasteBinFactory, as_guest: bool) -> Result<String, Box<Error>> {
    if as_guest {
        info!("preparing for guest post");
        pb_factory.user_key = None;
    } else if pb_factory.user_key.is_none() {
        match pb_factory.uncache_user_key() {
            Err(_) => pb_factory._login()?,
            _ => {}
        }
    }

    trace!("{:#?}", pb_factory);
    let post_res = pb_factory._post();
    match post_res {
        Ok(hash) => {
            info!("post worked from the first try...");
            return Ok(hash);
        }
        Err(e) => {
            if _should_login(&e) {
                info!("We need to login..first post has failed");
                pb_factory._login()?;
            } else {
                error!("Our first post has failed and not because we need login");
                return Err(e);
            }
        }
    }
    Ok(pb_factory._post()?)
}

impl PasteBinFactory {
    fn _login(&mut self) -> Result<(), Box<Error>> {
        let login = self.set_api_option(PasteBinOptions::Login)._finish()?;
        let mut res = login.do_request()?;
        get_pbin_response(&mut res).map(|val| {
            self.set_user_key(&val);
            self.cache_user_key()
                .map(|_| ())
                .map_err(|_| ())
                .unwrap_or(());
            ()
        })
    }

    fn _post(&mut self) -> Result<String, Box<Error>> {
        let post = self.set_api_option(PasteBinOptions::Paste)._finish()?;
        let mut res = post.do_request()?;
        get_pbin_response(&mut res)
    }

    pub fn cache_user_key(&self) -> Result<(), Box<Error>> {
        if self.user_key_cache.is_none() {
            return Err(Box::new(
                io::Error::new(io::ErrorKind::NotFound, "key cache not set"),
            ));
        }
        if self.user_key.is_none() {
            return Err(Box::new(
                io::Error::new(io::ErrorKind::NotFound, "user key not set"),
            ));
        }
        //OB-it
        let bytes = self.user_key.as_ref().unwrap().as_bytes();
        File::create(self.user_key_cache.as_ref().unwrap())?.write(
            bytes,
        )?;
        Ok(())
    }

    pub fn uncache_user_key(&mut self) -> Result<(), Box<Error>> {
        //Read and deob it
        if self.user_key_cache.is_none() {
            return Err(Box::new(
                io::Error::new(io::ErrorKind::NotFound, "key cache not set"),
            ));
        }
        let mut buff = Vec::new();
        File::open(self.user_key_cache.as_ref().unwrap())?
            .read_to_end(&mut buff)?;
        let val = String::from_utf8(buff)?;
        if val.len() > 1 {
            self.user_key = Some(val);
        }
        Ok(())
    }

    pub fn new() -> Self {
        PasteBinFactory {
            api_option: PasteBinOptions::Paste,
            dev_key: "".to_owned(),
            paste_key: None,
            paste_code: None,
            paste_private: None,
            paste_title: None,
            paste_expire_date: None,
            paste_format: None,
            user_key: None,
            user_key_cache: None,
            user_name: None,
            user_pass: None,
        }
    }

    pub fn set_dev_key(&mut self, key: &str) -> &mut Self {
        self.dev_key = String::from(key);
        self
    }

    pub fn set_paste_code(&mut self, paste_code: &str) -> &mut Self {
        self.paste_code = Some(String::from(paste_code));
        self
    }

    pub fn set_user_key_cache(&mut self, path: &str) -> &mut Self {
        self.user_key_cache = Some(Path::new(path).to_owned());
        self
    }

    pub fn set_private(&mut self, paste_type: PasteBinPrivate) -> &mut Self {
        self.paste_private = Some(paste_type);
        self
    }

    pub fn set_title(&mut self, paste_title: &str) -> &mut Self {
        self.paste_title = Some(String::from(paste_title));
        self
    }

    pub fn set_expire_date(&mut self, expire_date: PasteBinExpireDate) -> &mut Self {
        self.paste_expire_date = Some(expire_date);
        self
    }

    pub fn set_paste_format(&mut self, paste_format: PasteBinFormat) -> &mut Self {
        self.paste_format = Some(paste_format);
        self
    }

    pub fn set_user_key(&mut self, user_key: &str) -> &mut Self {
        self.user_key = Some(String::from(user_key));
        self
    }

    pub fn set_user_name(&mut self, user_name: &str) -> &mut Self {
        self.user_name = Some(String::from(user_name));
        self
    }

    pub fn set_user_pass(&mut self, user_pass: Vec<u8>) -> &mut Self {
        self.user_pass = Some(user_pass);
        self
    }

    pub fn set_api_option(&mut self, api_option: PasteBinOptions) -> &mut Self {
        self.api_option = api_option;
        self
    }

    pub fn set_paste_key(&mut self, paste_key: &str) -> &mut Self {
        self.paste_key = Some(paste_key.to_owned());
        self
    }

    fn _finish(&self) -> Result<PasteBinRequest, Box<Error>> {
        let url_body = self.build_rest_parms()?;
        let url = self.api_option.get_dst_url()?;
        let mut req = PasteBinRequest {
            url: url,
            headers: HeaderMap::new(),
            body: url_body,
        };
        req.headers.insert(
            CONTENT_TYPE,
            HeaderValue::from_static("application/x-www-form-urlencoded"),
        );
        //Not ok...displays sauce in clear
        //debug!("{:#?}", req);
        Ok(req)
    }

    fn _sanity(&self) -> bool {
        if self.dev_key.len() <= 1 {
            error!("dev_key should always be set");
            return false;
        }
        let res = match self.api_option {
            PasteBinOptions::Login => self.user_name.is_some() && self.user_pass.is_some(),
            PasteBinOptions::List => self.user_key.is_some(),
            PasteBinOptions::Delete => self.user_key.is_some() && self.paste_key.is_some(),
            PasteBinOptions::Paste => {
                let mut val = true;
                if let Some(ref private) = self.paste_private {
                    if private.to_owned() == PasteBinPrivate::Private {
                        val &= self.user_key.is_some();
                    }
                }
                self.paste_code.is_some() && val
            }
            PasteBinOptions::Raw => self.user_key.is_some() && self.paste_key.is_some(),
            PasteBinOptions::Trends => {
                error!("not implemented yet");
                false
            }
            PasteBinOptions::UserDetails => {
                error!("not implemented yet");
                //self.user_key.is_some()
                false
            }
        };
        res
    }

    fn _build_parms(&self) -> Result<String, Box<Error>> {
        let mut url_parms = form_urlencoded::Serializer::new(String::new());
        url_parms.append_pair("api_dev_key", self.dev_key.as_ref());
        url_parms.append_pair("api_option", &self.api_option.to_string());
        match self.api_option {
            PasteBinOptions::Delete => {
                url_parms.append_pair("api_user_key", self.user_key.as_ref().unwrap());
                url_parms.append_pair("api_paste_key", self.paste_key.as_ref().unwrap());
            }
            PasteBinOptions::List => {
                url_parms.append_pair("api_user_key", self.user_key.as_ref().unwrap());
                url_parms.append_pair("api_paste_key", self.paste_key.as_ref().unwrap());
            }
            PasteBinOptions::Login => {
                url_parms.append_pair("api_user_name", self.user_name.as_ref().unwrap());
                let res = String::from_utf8(deob(self.user_pass.as_ref().unwrap()))?;
                url_parms.append_pair("api_user_password", &res);
            }
            PasteBinOptions::Paste => {
                if self.user_key.is_some() {
                    url_parms.append_pair("api_user_key", self.user_key.as_ref().unwrap());
                }
                url_parms.append_pair("api_paste_code", self.paste_code.as_ref().unwrap());
                if let Some(ref expire_date) = self.paste_expire_date {
                    url_parms.append_pair("api_paste_expire_date", &expire_date.to_string());
                } else {
                    url_parms.append_pair(
                        "api_paste_expire_date",
                        &PasteBinExpireDate::TenMin.to_string(),
                    );
                }
                if self.paste_title.is_some() {
                    url_parms.append_pair("api_paste_name", self.paste_title.as_ref().unwrap());
                }
                if let Some(ref paste_format) = self.paste_format {
                    url_parms.append_pair("api_paste_format", &paste_format.to_string());
                }
                if self.paste_private.is_some() {
                    url_parms.append_pair(
                        "api_paste_private",
                        &self.paste_private.as_ref().unwrap().to_string(),
                    );
                }
            }
            PasteBinOptions::Raw => {
                url_parms.append_pair("api_user_key", self.user_key.as_ref().unwrap());
                url_parms.append_pair("api_paste_key", self.paste_key.as_ref().unwrap());
            }
            PasteBinOptions::Trends => {
                error!("unimplemented");
            }
            PasteBinOptions::UserDetails => {
                error!("unimplemented");
            }
        }
        let url_parms = url_parms.finish();
        Ok(url_parms)
    }

    pub fn build_rest_parms(&self) -> Result<String, Box<Error>> {
        if self._sanity() == false {
            error!("{:#?}", self);
            return Err(Box::new(io::Error::from(io::ErrorKind::InvalidData)));
        }
        let val = self._build_parms()?;
        Ok(val)
    }
}

#[derive(Debug, Clone)]
struct PasteBinRequest {
    url: Url,
    headers: HeaderMap,
    body: String,
}

impl PasteBinRequest {
    pub fn get_url(&self) -> Url {
        self.url.clone()
    }
    pub fn get_headers(&self) -> HeaderMap {
        self.headers.clone()
    }
    pub fn get_body(&self) -> String {
        self.body.clone()
    }
    pub fn do_request(&self) -> Result<Response, Box<Error>> {
        let req = Client::new()
            .post(self.get_url())
            .headers(self.get_headers())
            .body(self.get_body());
        let res = req.send()?;
        Ok(res)
    }

    pub fn parse_pastes(text: &str) -> Result<Vec<PasteBinEntry>, Box<Error>> {
        let mut v = Vec::new();
        let mut buff = Vec::new();
        let mut reader = Reader::from_str(text);
        let mut label = "";
        let mut entry = PasteBinEntry::new();
        loop {
            match reader.read_event(&mut buff) {
                Ok(Start(ref e)) => {
                    trace!(
                        "Matching start event with tag {:?}",
                        String::from_utf8_lossy(e.name())
                    );
                    if label.is_empty() == false {
                        error!("Label should be empty at this point");
                    }
                    match e.name() {
                        b"paste" => {
                            entry = PasteBinEntry::new();
                        }
                        b"paste_key" => label = "paste_key",
                        b"paste_title" => label = "paste_title",
                        _ => {}
                    }
                }
                Ok(End(ref e)) => {
                    label = "";
                    match e.name() {
                        b"paste" => {
                            trace!("Pushing PasteBinEntry with {:#?}", entry);
                            v.push(entry.clone());
                        }
                        _ => {}
                    }
                }
                Ok(Text(e)) => {
                    if label != "" {
                        trace!("Parsing text corresponding to label {:?}", label);
                        entry.set_val(label, &e.unescape_and_decode(&reader).unwrap());
                        label = "";
                    }
                }
                Ok(Eof) => break,
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                _ => {
                    trace!("Skipping event");
                }
            }
            buff.clear();
        }
        Ok(v)
    }
}

#[derive(Debug, Clone)]
pub struct PasteBinEntry {
    paste_key: String,
    paste_title: String,
    paste_code: String,
}

impl PasteBinEntry {
    pub fn new() -> PasteBinEntry {
        PasteBinEntry {
            paste_key: String::from(""),
            paste_title: String::from(""),
            paste_code: String::from(""),
        }
    }
    pub fn set_paste_key(&mut self, val: &str) {
        self.paste_key = val.to_owned();
    }
    pub fn get_paste_key(&self) -> &str {
        self.paste_key.as_ref()
    }
    pub fn set_paste_title(&mut self, val: &str) {
        self.paste_title = val.to_owned();
    }
    pub fn get_paste_title(&self) -> &str {
        self.paste_title.as_ref()
    }
    pub fn set_paste_code(&mut self, val: &str) {
        self.paste_code = val.to_owned();
    }
    pub fn get_paste_code(&self) -> &str {
        self.paste_code.as_ref()
    }
    pub fn set_val(&mut self, key: &str, val: &str) {
        match key {
            "paste_key" => self.set_paste_key(val),
            "paste_title" => self.set_paste_title(val),
            "paste_code" => self.set_paste_code(val),
            _ => panic!("key is fucked up {}", key),
        }
    }
}
