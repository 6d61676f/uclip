use std::error::Error;
use std::fmt;
use url::Url;

#[derive(Debug, Clone, PartialEq)]
pub enum PasteBinPrivate {
    Public,
    Unlisted,
    Private,
}

#[derive(Debug, Clone)]
pub enum PasteBinOptions {
    Paste,
    Trends,
    List,
    Raw,
    UserDetails,
    Delete,
    Login,
}

#[derive(Debug, Clone)]
pub enum PasteBinExpireDate {
    Never,
    TenMin,
    OneHour,
    OneDay,
    OneWeek,
    TwoWeeks,
    OneMonth,
    SixMonths,
    OneYear,
}

#[derive(Debug, Clone)]
pub enum PasteBinFormat {
    Text,
}

impl fmt::Display for PasteBinOptions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            PasteBinOptions::Paste => "paste",
            PasteBinOptions::Trends => "trends",
            PasteBinOptions::List => "list",
            PasteBinOptions::Raw => "show_paste",
            PasteBinOptions::UserDetails => "userdetails",
            PasteBinOptions::Delete => "delete",
            PasteBinOptions::Login => "login",
        };
        write!(f, "{}", val.to_owned())
    }
}

impl PasteBinOptions {
    pub fn get_dst_url(&self) -> Result<Url, Box<Error>> {
        let var = match self {
            PasteBinOptions::Delete
            | PasteBinOptions::List
            | PasteBinOptions::Paste
            | PasteBinOptions::Trends
            | PasteBinOptions::UserDetails => "https://pastebin.com/api/api_post.php",
            PasteBinOptions::Login => "https://pastebin.com/api/api_login.php",
            PasteBinOptions::Raw => "https://pastebin.com/api/api_raw.php",
        };
        Ok(Url::parse(var)?)
    }
}

impl fmt::Display for PasteBinFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            PasteBinFormat::Text => "text",
        };
        write!(f, "{}", val.to_owned())
    }
}

impl fmt::Display for PasteBinExpireDate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            PasteBinExpireDate::Never => "N",
            PasteBinExpireDate::TenMin => "10M",
            PasteBinExpireDate::OneHour => "1H",
            PasteBinExpireDate::OneDay => "1D",
            PasteBinExpireDate::OneWeek => "1W",
            PasteBinExpireDate::TwoWeeks => "2W",
            PasteBinExpireDate::OneMonth => "1M",
            PasteBinExpireDate::SixMonths => "6M",
            PasteBinExpireDate::OneYear => "1Y",
        };
        write!(f, "{}", val.to_owned())
    }
}

impl fmt::Display for PasteBinPrivate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            PasteBinPrivate::Public => "0",
            PasteBinPrivate::Unlisted => "1",
            PasteBinPrivate::Private => "2",
        };
        write!(f, "{}", val.to_owned())
    }
}
