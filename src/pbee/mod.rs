pub mod types;
use pbee::types::*;
use reqwest::header::{HeaderMap, CONTENT_TYPE};
use reqwest::{Client, ClientBuilder, Url};
use std::error::Error;
use std::sync::{Arc, Mutex};
use utils::deob;

type ClientHandler = Arc<Mutex<Client>>;
pub trait Request {
    fn do_it(&mut self) -> Result<ResponseType, Box<Error>>;
}

pub struct Factory {
    key: Vec<u8>,
    kind: RequestType,
    client: ClientHandler,
}

impl Factory {
    pub fn new(key: Vec<u8>, kind: RequestType) -> Result<Factory, Box<Error>> {
        Ok(Factory {
            key,
            kind,
            client: Arc::new(Mutex::new(ClientBuilder::new().build()?)),
        })
    }

    pub fn set_kind(&mut self, kind: RequestType) -> &mut Self {
        self.kind = kind;
        self
    }

    pub fn build(&self) -> Result<Box<Request>, Box<Error>> {
        let val: Box<Request> = match &self.kind {
            RequestType::Post(ref t) => Box::new(_Post::new(t, &self.key, self.client.clone())?),
            RequestType::Delete(ref t) => {
                error!("NO WORKING --  08.11.2018");
                Box::new(_Delete::new(t, &self.key, self.client.clone())?)
            }
            RequestType::Get(ref t) => {
                unimplemented!();
                //Box::new(_Get::new(t, &self.key, self.client.clone()))
            }
            RequestType::List(ref t) => {
                unimplemented!();
                //Box::new(_List::new(t, &self.key,self.client.clone()))
            }
        };
        Ok(val)
    }
}

struct _Post {
    client: ClientHandler,
    url: Url,
    header: HeaderMap,
    body: String,
}

impl _Post {
    fn new(datum: &PastePost, key: &[u8], client: ClientHandler) -> Result<_Post, Box<Error>> {
        let url = Url::parse("https://api.paste.ee/v1/pastes")?;
        let mut header = HeaderMap::new();
        header.insert(CONTENT_TYPE, "application/json".parse()?);
        header.insert("X-Auth-Token", String::from_utf8(deob(key))?.parse()?);
        let body = serde_json::to_string(datum)?;
        let post = _Post {
            url,
            header,
            body,
            client,
        };
        debug!("{:#?} {:#?}", post.url, post.body);
        Ok(post)
    }
}

impl Request for _Post {
    fn do_it(&mut self) -> Result<ResponseType, Box<Error>> {
        let c_lock = self.client.lock().expect("ClientRequest lock has failed");
        let mut res = c_lock
            .post(self.url.clone())
            .headers(self.header.clone())
            .body(self.body.clone())
            .send()?;
        if res.status().is_success() == false {
            error!("{:?}", res.status());
        }
        let res: PostResponse = res.json()?;
        Ok(ResponseType::Post(res))
    }
}

//NOTE: It does not fucking work
//Neither this, nor GET -- tried both with curl and dev-wise
struct _Delete {
    client: ClientHandler,
    url: Url,
    header: HeaderMap,
}

impl _Delete {
    pub fn new(id: &str, key: &[u8], client: ClientHandler) -> Result<_Delete, Box<Error>> {
        let url = Url::parse("https://api.paste.ee/v1/pastes/")?.join(id)?;
        let mut header = HeaderMap::new();
        header.insert("X-Auth-Token", String::from_utf8(deob(key))?.parse()?);
        let del = _Delete {
            client,
            url,
            header,
        };
        debug!("{:#?}", del.url);
        Ok(del)
    }
}

impl Request for _Delete {
    fn do_it(&mut self) -> Result<ResponseType, Box<Error>> {
        let c_lock = self.client.lock().expect("ClientRequest lock has failed");
        let mut res = c_lock
            .delete(self.url.clone())
            .headers(self.header.clone())
            .send()?;
        if res.status().is_success() == false {
            error!("{:?} {:?}", res.status(), res.text()?);
        }
        Ok(ResponseType::Delete(res.json()?))
    }
}

struct _List {}

struct _Get {}
