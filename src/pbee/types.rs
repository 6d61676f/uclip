#[derive(Debug, Clone)]
pub enum RequestType {
    Post(PastePost), //POST https://api.paste.ee/v1/pastes
    List(PasteList), //GET https://api.paste.ee/v1/pastes
    Delete(String),  //DELETE https://api.paste.ee/v1/pastes/<id>
    Get(String),     //GET https://api.paste.ee/v1/pastes/<id>
}

#[derive(Debug, Clone)]
pub enum ResponseType {
    Post(PostResponse),
    List(ListResponse),
    Delete(DeleteResponse),
    Get(Item),
}

pub struct Payload {
    paste: Option<PastePost>,
    id: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct PostResponse {
    pub id: String,
    pub link: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct PastePost {
    pub encrypted: Option<bool>,
    pub description: String,
    pub sections: Vec<Section>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Section {
    pub name: Option<String>,
    pub syntax: String,
    pub contents: String,
}

#[derive(Debug, Clone, Serialize)]
pub struct PasteList {
    pub perpage: u32,
    pub page: u32,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ListResponse {
    total: u32,
    per_page: u32,
    current_page: u32,
    last_page: u32,
    next_page_url: String,
    prev_page_url: String,
    from: u32,
    to: u32,
    data: Vec<Item>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Item {
    id: String,
    description: String,
    views: u32,
    created_at: String,
    sections: Vec<Section>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DeleteResponse {
    success: String,
}
