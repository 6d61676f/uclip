#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate reqwest;
extern crate url;
extern crate quick_xml;
extern crate crypto;
extern crate rand;
extern crate serde_json;
extern crate serde;

pub trait Pastie {
    fn post(body: String) -> Result<String, Box<std::error::Error>>;
}

pub mod pbin;
pub mod pbee;
pub mod utils;