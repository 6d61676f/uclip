extern crate uclip;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate rand;

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use std::error::Error;
use std::fs;
use std::fs::File;
use std::io::Read;
use uclip::pbee::types::*;
use uclip::pbee::*;
use uclip::pbin::types::*;
use uclip::pbin::*;

fn main() -> Result<(), Box<Error>> {
    env_logger::init();
    //let rand_str: String = thread_rng().sample_iter(&Alphanumeric).take(30).collect();
    // let dev_key = fs::read_to_string("/opt/.pastebin.key")?;
    // let dev_key = dev_key.trim();
    // let ron_jeremy: String = String::from("sarpedon");
    // let mut celine_dion = Vec::new();
    // File::open("/opt/.locales")?.read_to_end(&mut celine_dion)?;
    // let mut pb_def = PasteBinFactory::new()
    //     .set_user_name(&ron_jeremy)
    //     .set_user_pass(celine_dion)
    //     .set_dev_key(&dev_key)
    //     .set_user_key_cache("./.cache.key")
    //     .set_paste_code(&rand_str)
    //     .set_api_option(PasteBinOptions::Paste)
    //     .set_title("fml..")
    //     .clone();
    // let val = try_post(&mut pb_def, false)?;
    // println!("{}", val);
    // //let mut res;
    // ////3. List
    // //let pb_list = pb_def
    // //    .clone()
    // //    .set_api_option(PasteBinOptions::List)
    // //    .finish()?;
    // //res = pb_list.do_request()?;
    // //let body = res.text()?;
    // ////4. Parse
    // //let vals = PasteBinRequest::parse_pastes(&body)?;
    // //println!("{:?}", vals);
    // ////Strange behavior when trying to overwrite exiting paste...
    // ////If there's a timeout a new paste with the same name is created...otherwise nothing happens
    // ////Try and cache the user_key before reposting
    // ////5. Retrieve
    // //let pb_retrieve = pb_def
    // //    .clone()
    // //    .set_api_option(PasteBinOptions::Raw)
    // //    .set_paste_key(vals.get(0).ok_or("index is ded")?.get_paste_key())
    // //    .finish()?;
    // //res = pb_retrieve.do_request()?;
    // //let body = res.text()?;
    // //println!("{:?}", body);

    let mut celine_dion = Vec::new();
    File::open("/opt/.pastee.key")?.read_to_end(&mut celine_dion)?;
    let typ = RequestType::Post(PastePost {
        encrypted: None,
        description: "fml".to_string(),
        sections: vec![Section {
            name: None,
            syntax: "autodetect".to_string(),
            contents: "W0l0l0".to_string(),
        }],
    });
    let typ2 = RequestType::Delete(String::from("dwT44"));
    let mut fact = Factory::new(celine_dion, typ)?;
    let mut req = fact.build()?;
    //let res = req.do_it();
    //println!("{:?}", res);

    let mut req = fact.set_kind(typ2).build()?;
    let res = req.do_it()?;
    println!("{:?}", res);

    Ok(())
}
