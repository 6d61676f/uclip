use crypto::buffer::{BufferResult, ReadBuffer, WriteBuffer};
use crypto::digest::Digest;
use crypto::sha2::Sha256;
use crypto::{aes, blockmodes, buffer, symmetriccipher};
use std::error::Error;
use std::fs;
use std::io::{self, Read};
use std::path::PathBuf;

pub fn sha256sum(data: &[u8]) -> Vec<u8> {
    let mut hasher = Sha256::new();
    hasher.input(data);
    hasher.result_str().into_bytes()
}

pub fn encrypt(
    data: &[u8],
    key: &[u8],
    iv: &[u8],
) -> Result<Vec<u8>, symmetriccipher::SymmetricCipherError> {
    let mut encryptor =
        aes::cbc_encryptor(aes::KeySize::KeySize256, key, iv, blockmodes::PkcsPadding);
    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = buffer::RefReadBuffer::new(data);
    let mut buffer = [0; 4096];
    let mut write_buffer = buffer::RefWriteBuffer::new(&mut buffer);
    loop {
        let result = encryptor.encrypt(&mut read_buffer, &mut write_buffer, true)?;
        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .map(|&i| i),
        );
        match result {
            BufferResult::BufferOverflow => {}
            BufferResult::BufferUnderflow => break,
        }
    }
    Ok(final_result)
}

pub fn decrypt(
    data: &[u8],
    key: &[u8],
    iv: &[u8],
) -> Result<Vec<u8>, symmetriccipher::SymmetricCipherError> {
    let mut decryptor =
        aes::cbc_decryptor(aes::KeySize::KeySize256, key, iv, blockmodes::PkcsPadding);
    let mut final_result = Vec::<u8>::new();
    let mut read_buffer = buffer::RefReadBuffer::new(data);
    let mut buffer = [0; 4096];
    let mut write_buffer = buffer::RefWriteBuffer::new(&mut buffer);
    loop {
        let result = decryptor.decrypt(&mut read_buffer, &mut write_buffer, true)?;
        final_result.extend(
            write_buffer
                .take_read_buffer()
                .take_remaining()
                .iter()
                .map(|&i| i),
        );
        match result {
            BufferResult::BufferOverflow => {}
            BufferResult::BufferUnderflow => break,
        }
    }
    Ok(final_result)
}

pub fn deob(bytes: &[u8]) -> Vec<u8> {
    let mut vec = Vec::new();
    vec.resize(bytes.len() - 1, 0);

    //Evens
    {
        let mut delim = 0;
        while bytes[delim] != 0x1c {
            delim += 1;
        }

        let mut odd = 0;
        let mut even = 0;
        let mut even_index = delim + 1;

        while even_index < bytes.len() {
            let mut tmp: u8 = bytes[even_index];
            let next_odd: u16 = bytes[odd] as u16;
            if next_odd == 0x1c {
                tmp ^= 0x47;
            } else {
                let mut j: u16 = 1;
                while j <= next_odd {
                    tmp ^= j as u8;
                    j <<= 3;
                }
            }
            vec[even] = tmp;
            even_index += 1;
            odd += 1;
            even += 2;
        }
    }

    //Odds
    {
        let mut index = 0;
        let mut odd_index = 1;
        while index < bytes.len() && bytes[index] != 0x1c {
            let mut tmp: u8 = bytes[index];
            if (tmp ^ (0x56 - 1)) == 0x1c {
                tmp ^= 0x56 - 1;
            }
            tmp ^= 0x65 - 1;
            vec[odd_index] = tmp;
            odd_index += 2;
            index += 1;
        }
    }
    vec
}

pub fn read_input(src: Option<PathBuf>) -> Result<String, Box<Error>> {
    let mut lines = String::new();
    match src {
        Some(ref path) => {
            debug!("Reading from path {:?}", path);
            lines = fs::read_to_string(path)?;
        }
        None => {
            debug!("Reading from stdin");
            io::stdin().read_to_string(&mut lines)?;
        }
    }
    Ok(lines)
}
